import { RomanJsPage } from './app.po';

describe('roman-js App', function() {
  let page: RomanJsPage;

  beforeEach(() => {
    page = new RomanJsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
