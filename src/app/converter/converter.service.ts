import { Injectable } from '@angular/core';

@Injectable()
export class ConverterService {

  constructor() { }

  public convertToRoman(arabic: number): string {

    let roman = '';
    let target = 10;

    let full = Math.trunc(arabic / target);

    for (let i = 0; i < full; i++) {
        roman += 'X';
      arabic -= target;
    }

    let c = this.computeArabicNextToTarget(target, arabic);
    roman += c.roman;
    arabic = c.arabic;

    full = Math.trunc(arabic / target);

    for (let i = 0; i < full; i++) {
        roman += 'X';
      arabic -= target;
    }

    target = 5;

    c = this.computeArabicNextToTarget(target, arabic);
    roman += c.roman;
    arabic = c.arabic;

    full = Math.trunc(arabic / target);

    for (let i = 0; i < full; i++) {
        roman += 'V';
      arabic -= target;
    }
    for (let i = 0; i < arabic; i++) {
        roman += 'I';
    }

    return roman;
  }

  public computeArabicNextToTarget(target: number, arabic: number): {roman: string, arabic: number} {
    let roman = '';
    const rest = arabic % target;
    if (rest + 1 === target) {
      roman += 'I';
      arabic++;
    }
    return {
      roman, arabic
    };

  }
}
