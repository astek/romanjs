/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ConverterService } from './converter.service';

describe('ConverterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConverterService]
    });
  });

  it('should exist', inject([ConverterService], (service: ConverterService) => {
    expect(service).toBeTruthy();
  }));

  it('should return I', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(1)).toBe('I');
  }));

  it('should return II', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(2)).toBe('II');
  }));

  it('should return III', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(3)).toBe('III');
  }));

  it('should return IV', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(4)).toBe('IV');
  }));

  it('should return V', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(5)).toBe('V');
  }));

  it('should return VIII', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(8)).toBe('VIII');
  }));

  it('should return XIII', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(13)).toBe('XIII');
  }));

  it('should return XVI', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(16)).toBe('XVI');
  }));

  it('should return IV', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(9)).toBe('IX');
  }));

  it('should return XIX', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(19)).toBe('XIX');
  }));

  it('should return XXX', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(30)).toBe('XXX');
  }));
  
  /*it('should return XL', inject([ConverterService], (service: ConverterService) => {
    expect(service.convertToRoman(40)).toBe('XL');
  }));*/

});
